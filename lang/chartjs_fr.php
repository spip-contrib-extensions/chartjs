<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;
 
$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'champ_charger_scripts_label' => 'Chargement des scripts de Chart.js',
	'champ_charger_scripts_explication' => 'Charger systématiquement les scripts Chart.js ? Par défaut, ils sont chargés uniquement lors de l’utilisation du modèle <code>chart</code>. Pour des usages plus spécifiques, vous pouvez les charger systématiquement sur toutes les pages.',
	'champ_charger_scripts_public_case' => 'Charger Chart.js systématiquement dans l’espace public',
	'champ_charger_scripts_prive_case' => 'Charger Chart.js systématiquement dans l’espace privé',

	// D
	'demo_bar' => 'Barre',
	'demo_bubble' => 'Bulles',
	'demo_doughnut' => 'Beignet',
	'demo_line' => 'Courbe',
	'demo_pie' => 'Camembert',
	'demo_polaire' => 'Polaire',
	'demo_radar' => 'Radar',
	'demo_scatter' => 'Nuage de points (ou diagramme de dispersion)',
	'demo_spip_bigdata' => 'Graphes dynamiques avec boucle SPIP DATAS',
	'demo_spip_data' => 'Graphes dynamiques avec boucle SPIP',
	'demo_spip_legend' => 'Cours du Yen en Euro des 30 derniers jours',
	'demo_titre' => 'Démonstration du plugin Chart.js',
	'demo_titre_exemples_avances' => 'Exemples avancés',
	'demo_titre_types' => 'Types de graphiques',
	'demo_type' => 'Type: @type@',

	// T
	'titre_formulaire_configurer_chartjs' => 'Configuration',
	'titre_page_configurer_chartjs' => 'Configurer Chart.js',
);